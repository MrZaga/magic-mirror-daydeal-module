from bs4 import BeautifulSoup
import requests

url = 'https://www.daydeal.ch/'
htmlString = requests.get(url).text
html = BeautifulSoup(htmlString, 'lxml')

#Product class scraping
title1_full = html.find('h1', {'class':'product-description__title1 d-none d-lg-block'})
title1 = str(title1_full.contents)[2:-2]


#Product name scraping
title2_full = html.find('h2', {'class':'product-description__title2 d-none d-lg-block'})
title2 = str(title2_full.contents)[2:-2]

#Product price new scraping
price_new_full = html.find('h2', {'class':'product-pricing__prices-new-price js-deal-price'})
price_new = str(price_new_full.contents)[2:-4]

#Product price old scraping
price_old_full = html.find('span', {'class':'js-old-price'})
price_old = str(price_old_full.contents)[2:-4]

#product discount scraper
discount_full = html.find('span', {'class':'js-pricetag'})
discount = str(discount_full.contents)[2:-2]

#write to html file
daydeal_html = '''
<!DOCTYPE html>
<html lang="de">
  <head>
    <meta charset="utf-8" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;400&display=swap" rel="stylesheet">
    <link href="style.css" rel="stylesheet" type="text/css"> 
  </head>
  <body>
  <h1>{}</h1>
  <h2>{}</h2>
  <p>{}.- statt {}.- (-{}%)</p>
  </body>
</html>
'''.format(title1,title2, price_new, price_old, discount)

Html_file= open("daydeal.html","w")
Html_file.write(daydeal_html)
Html_file.close()